<style>
    .input-group-text i{
        font-size: 50px;
    }
</style>
<div id="list-product" data-action="{{route('products.get-list')}}">
    <div class="content">
        <table class="data-table table nowrap">
            <thead>
            <tr>
                <th>#</th>
                <th>Thumbnail</th>
                <th>Name</th>
                <th>Description</th>
                <th>Color</th>
                <th>Price</th>
                <th class="datatable-nosort">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product )
                <tr>
                    <td>{{$loop->iteration + (($products->currentPage() -1)* $products->perPage())}}</td>
                    <td><img src="{{URL::to('/')}}/admin/products/img/{{$product->thumbnail}}" class="img-thumbnail" width="75" alt=""></td>
                    <td>{{$product->thumbnail}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->description}}</td>
                    <td>{{$product->color}}</td>
                    <td>{{$product->price}}&nbsp;VNĐ</td>
                    <td style="width:15%;">
                        <button type="button" class="btn btn-success edit_product"
                                data-action="{{route('products.show',$product->id)}}"
                                data-url="{{route('products.update',['id'=>$product->id])}}">Edit
                        </button>
                        <button type="button" class="btn btn-danger detete_product"
                                data-action="{{route('products.delete',$product->id)}}">Delete
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $products->links()}}
</div>


