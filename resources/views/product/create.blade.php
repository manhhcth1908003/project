<div class="modal fade" id="productAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Product</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_create_product">
                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Name</label>
                        <input placeholder="Enter Name" type="text" class="form-control" name="name"
                               id="recipient-name">
                        <div class="alert alert-danger" id="error_name" style="display: none"></div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Description</label>
                            <textarea placeholder="Enter Description" class="form-control" name="description"
                                      id="message-text"></textarea>
                            <div class="alert alert-danger" id="error_description" style="display: none"></div>
                        </div>
                    </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Color</label>
                            <input placeholder="Enter Color" type="text" class="form-control" name="color"
                                   id="recipient-name">
                            <div class="alert alert-danger" id="error_color" style="display: none"></div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Price</label>
                            <input placeholder="Enter Price" type="text" class="form-control" name="price"
                                   id="recipient-name">
                            <div class="alert alert-danger" id="error_price" style="display: none"></div>
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="recipient-name" class="col-form-label">Thumbnail</label>--}}
{{--                            <input placeholder="Enter Thumbnail" type="text" class="form-control" name="thumbnail"--}}
{{--                                   id="recipient-name">--}}
{{--                            <div class="alert alert-danger" id="error_name" style="display: none"></div>--}}
{{--                        </div>--}}

                    <div class="form-group">
                        <button type="button" id="upload_widget" class="btn btn-success">Upload
                            files
                        </button>
                        <div class="thumbnail">
                        </div>
                    </div>



{{--                    <input type="file" id="file-input" onchange="loadPreview(this)" name="image[]"/>--}}
{{--                    <div id="thumb-output"></div>--}}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="create_products"
                        data-url="{{route('products.store')}}"
                >Save Product
                </button>
            </div>
        </div>
    </div>
</div>



