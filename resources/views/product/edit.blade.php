<div class="modal fade" id="productUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Updae product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_update_product" method="post">
                    @method('put')
                    @csrf
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Name</label>
                        <input placeholder="Enter Name" type="text" class="form-control name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label ">Description</label>
                        <textarea placeholder="Enter Description" class="form-control description" name="description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Name</label>
                        <input placeholder="Enter Name" type="text" class="form-control color" name="color">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label ">Price</label>
                        <input placeholder="Enter Price" type="text" class="form-control price" name="price">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Thumbnail</label>
                        <input placeholder="Enter Thumbnail" type="text" class="form-control thumbnail"
                               name="thumbnail">
                    </div>
                    {{--                    <div class="form-group">--}}
                    {{--                        <div class="custom-file">--}}
                    {{--                            <input type="file" class="custom-file-input" id="inputGroupFile04">--}}
                    {{--                            <label class="custom-file-label" for="inputGroupFile04">Choose file</label>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary update_product" id="update_products"
                >Save Product</button>
            </div>
        </div>
    </div>
</div>


