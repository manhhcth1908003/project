@extends('layout.admin.master')
@section('content')

    <div class="pd-ltr-20">

        <div id="list" class="card-box mb-30 table-hover">
            <h2 class="h4 pd-20">Best Selling Products |
                <button id="open_modal_create" type="button" class="btn btn-info" data-toggle="modal"
                        data-target="#productAddModal">Create Product </button>
            </h2>
            <form data-action="{{route('products.search')}}" id="form-search_product">
                <div class="input-group md-form form-sm form-1 pl-0">
                    <div class="input-group-prepend">
                            <span  class="input-group-text purple lighten-3" id="basic-text1"><i
                                    class="search-toggle-icon dw dw-search2" aria-hidden="true"></i></span>
                    </div>
                    <input id="btn_search_product"
                           class="form-control my-0 py-1" name="name" type="text" placeholder="Search by name"
                           aria-label="Search">
                </div>
            </form>
            <div id="list-product" data-action="{{route('products.get-list')}}"></div>
        </div>


        @include('product.edit')
        @include('product.create')
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="admin/products/index.js"></script>
@endsection

