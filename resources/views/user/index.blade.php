@extends('layout.admin.master')
@section('content')
    <div class="pd-ltr-20">

        <div id="list" class="card-box mb-30 table-hover">
            <h2 class="h4 pd-20">Best Selling Products |
                <button id="open_modal_create" type="button" class="btn btn-info" data-toggle="modal"
                        data-target="#productAddModal">Create User
                </button>
            </h2>
            <div id="list-product" ></div>
        </div>

        <div class="content">
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created_at</th>
                    <th>Update_at</th>
                    <th>Status</th>
                    <th class="datatable-nosort">Action</th>
                </tr>
                </thead>
                <tbody>

                {{--            @foreach($products as $product )--}}
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="width:10%;">
                        <button type="button" class="btn btn-success edit_product">Edit</button>
                        <button type="button" class="btn btn-danger detete_product">Delete</button>
                    </td>
                </tr>
                {{--            @endforeach--}}
                </tbody>
            </table>
        </div>
    </div>

@endsection
