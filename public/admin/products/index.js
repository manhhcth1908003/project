const GET_METHOD = 'GET';
const POST_METHOD = 'POST';
const PUT_METHOD = 'PUT';
const DELETE_METHOD = 'DELETE';

function callProductApi(url, data, method = GET_METHOD) {
    return $.ajax({
        method: method,
        data: data,
        url: url
    });
}

function getList() {
    let url = $('#list-product').data('action');
    callProductApi(url)
        .then((res) => {
            $('#list-product').replaceWith(res);
        })
}

function list() {
    let url = $('#form-search_product').data('action');
    let data = $('#form-search_product').serialize();
    list(url, data);
}

//create
$(document).ready(function () {
    getList();
    let url_updade;
    $(document).on('click', '#create_products', function () {
        let data = $('#form_create_product').serialize();
        let url_insert = $(this).data('url');
        callProductApi(url_insert, data, POST_METHOD)
            .then(() => {
                getList();
                swal("Success!", "You just created a new product!", "success");
                $('#productAddModal').modal('hide');
            })
            .catch((res) => {
                var errors = $.parseJSON(res.responseText);
                $.each(errors, function (key, val) {
                    if (val.name) {
                        $('#error_name').html(res.responseJSON.errors.name);
                        $('#error_name').css('display', 'block');
                    }
                    if (val.description) {
                        $('#error_description').html(res.responseJSON.errors.description);
                        $('#error_description').css('display', 'block');
                        console.log(val)
                    }
                    if (val.price) {
                        $('#error_price').html(res.responseJSON.errors.price);
                        $('#error_price').css('display', 'block');
                    }
                    if (val.color) {
                        $('#error_color').html(res.responseJSON.errors.color);
                        $('#error_color').css('display', 'block');
                    }

                })
            })

        // swal("Fail!", "You just created a new product!", "warning");
    })

//update:
    $(document).on('click', '.edit_product', function () {
        let url = $(this).data('action')
        url_updade = $(this).data('url')
        callProductApi(url)
            .then((res) => {
                $('.name').val(res.product.name)
                $('.description').val(res.product.description)
                $('.color').val(res.product.color)
                $('.price').val(res.product.price)
                $('.thumbnail').val(res.product.thumbnail)
                $('#productUpdateModal').modal('show');
            })
    })
    $(document).on('click', '.update_product', function () {
        let data = $('#form_update_product').serialize()
        callProductApi(url_updade, data, PUT_METHOD)
            .then(() => {
                getList()
                $('#productUpdateModal').modal('hide')
                $('.modal-backdrop').css('display', 'none');
                swal("Good job!", "You clicked the button!", "success");
            })
        swal("Fail!", "You just created a new product!", "warning");
    })
//delete
    $(document).on('click', '.detete_product', function () {
        let url = $(this).data('action')
        callProductApi(url, {}, POST_METHOD)
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    getList()
                    swal("Poof! Your imaginary file has been deleted!", {
                        icon: "success",
                    });
                } else {
                }
            });

    })
//search
    $(document).on('keyup', '#btn_search_product', function () {
        let formSearch = $('#form-search_product');
        let data = formSearch.serialize();
        let url = formSearch.data('action');
        callProductApi(url, data)
            .then((res) => {
                $('#list-product').replaceWith(res);
            })
    })

})



$(document)

//thumbnail

//paginate
$(document).on('click', '.page-item a', function (event) {
    event.preventDefault();
    let url = $(this).attr('href')
    callProductApi(url)
        .then((res) => {
            $('#list-product').replaceWith(res);
        })
})


