<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateProduct extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'description' => 'required|min:3|max:100',
            'price' => 'required',
            'color' => 'required|min:3|max:100'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'không được bỏ trống!!',
            'name.required.min' => 'toi thieu 3 ki tu',
            'name.required.max' => 'toi da 100 ki tu',
            'description.required' => 'không được bỏ trống!!',
            'description.required.min' => 'toi thieu 3 ki tu',
            'description.required.max'=>'toi da 100 ki tu' ,
            'price.required' => 'không được bỏ trống!!',
            'color.required' => 'không được bỏ trống!!',
            'color.required.min' => 'toi thieu 3 ki tu',
            'color.required.max'=>'toi da 100 ki tu'
        ];
    }

    //    public function attributes()
    //    {
    //        return [
    //            'name' => 'ten danh muc sp',
    //        ];
    //    }
}
