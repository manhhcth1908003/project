<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateProduct;
use App\Product;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    public function _construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {

        return view('product.index');
    }

    public function getList()
    {
        $products = Product::orderby('id', 'DESC')->paginate(7);
        return view('product.list')->with('products', $products);
    }

    public function store(ValidateProduct $request)
    {   $request->validated();
        $product = Product::create($request->all());
        return response()->json([
            'data' => $product,
            'status' => 201
        ]);
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return response()->json([
            'message' => 'get data success',
            'product' => $product,
            'status' => 200
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->all());
        return response()->json([
            'message' => 'get data success',
            'product' => $product,
            'status' => 200
        ], 200);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete($id);
    }

    public function search(Request $request)
    {
        $data['name'] = $request->name;
        $products = Product::search($data);
        return view('product.list', compact('products'));
    }

}
