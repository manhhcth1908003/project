<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'description', 'thumbnail', 'color', 'price'];

    public function category()
    {
        return $this->hasMany('App\Category', 'category_id');
    }

    static public function search($data)
    {
        return self::withName($data['name'])->paginate(7);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', "%$name%") : null;
    }


}
