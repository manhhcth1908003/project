<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/products', 'ProductController@index')->name('products.index');
Route::post('/products/store', 'ProductController@store')->name('products.store');
Route::get('/products/store','ProductController@getList')->name('products.get-list');
Route::get('/products/show/{id}', 'ProductController@show')->name('products.show');
Route::put('/products/update/{id}','ProductController@update')->name('products.update');
Route::post('/products/delete/{id}','ProductController@destroy')->name('products.delete');
Route::get('products/search','ProductController@search')->name('products.search');


Route::resource('/users','UserController');
